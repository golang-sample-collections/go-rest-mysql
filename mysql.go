package go_rest_mysql

/**
 * Created by GoLand.
 * Project : go-rest-mysql
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-12-17
 * Time: 17:01
 */

import (
	"database/sql"
	"log"
)

func connect() *sql.DB {
	db, err := sql.Open("mysql", "xxx:yyy@tcp(zzz:3306)/database")

	if err != nil {
		log.Fatal(err)
	}

	return db
}
